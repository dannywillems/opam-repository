#! /bin/sh

alpine_version=3.10
hidapi_version=0.9.0

ocaml_version=4.07.1

opam_version=2.0.3
opam_tag=2.0.3
